# EAZYBLOG REACT WEB APPLICATION

The blog app is a platform for individuals to share their thoughts, ideas, and experiences with a wider audience. It allows users to create and publish their own blog posts, as well as browse and comment on posts written by others. The app aims to provide a user-friendly and engaging experience for both writers and readers.

## Features:
- User registration and login
- Blog post creation and editing
- Blog post publishing and unpublishing
- Blog post browsing and searching
- Commenting on blog posts
- Liking blog posts
- User profile

## Link:

💻 https://eazyblogapp.netlify.app/

## Tech
- Client : React, Redux, Quill
- Authentication: Firebase
- Database : Firebase

## Installation

- Clone the Repository
- Use below commands for both the folders
	1. ```npm install```
	2. ```npm run start```

## Environment Variables

```REACT_APP_API_KEY```
```REACT_APP_AUTH_DOMAIN```
```REACT_APP_PROJECT_ID```
```REACT_APP_STORAGE```
```REACT_APP_MESSANGING_ID```
```REACT_APP_APP_ID```
```REACT_APP_HOSTING```
