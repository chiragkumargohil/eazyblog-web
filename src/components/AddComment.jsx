import React, { useState } from "react";
import { useDispatch } from "react-redux";

import { addComment } from "../redux/actions/blogsAction";
import { currentDate } from "../utils";

export default function AddComment({ isAuth, id }) {
    const [comment, setComment] = useState("");
    const dispatch = useDispatch();

    const submitComment = (e) => {
        e.preventDefault();
        const data = {
            comment,
            uid: isAuth.uid,
            dated: currentDate(),
            displayName: isAuth.displayName,
            comments: []
        };
        dispatch(addComment(id, data));
        setComment("");
    };

    const dis = isAuth ? false : true;

    return (
        <div className="comment-box">
            <form onSubmit={submitComment}>
                <div className="flexed for-comments">
                    <h3>Comments</h3>
                    <button type="submit" id="add-comment" disabled={dis}>{!dis ? "Post Comment" : "Login to Post Comment"}</button>
                </div>
                <textarea name="comment" id="type-comment" className="comment" value={comment} onChange={(e) => setComment(e.target.value)} placeholder="Comment here..." disabled={dis} required></textarea>
            </form>
        </div>
    );
}