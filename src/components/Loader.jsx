import React from "react";

export default function Loading() {
    return (
        <div id="loader"><img src="https://media.tenor.com/On7kvXhzml4AAAAj/loading-gif.gif" alt="Loading..." /></div>
    );
}