import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../context/AuthProvider";
import { dislikeBlog, likeBlog } from "../redux/actions/blogsAction";
import { love, comment, share, loveFill } from "../assets";
import { toast } from "react-toastify";

export default function LikeCommentBTN({ blogId, likes, comments }) {
    const { isAuth } = useAuth();
    const dipatch = useDispatch();
    const navigate = useNavigate();
    const isLiked = likes?.includes(isAuth?.uid) || false;
    const dis = isAuth ? false : true;

    const handleLike = async (blogId) => {
        if (!isLiked) {
            dipatch(likeBlog(blogId, isAuth.uid));
        } else {
            dipatch(dislikeBlog(blogId, isAuth.uid));
        }
    };

    return (
        <div className="subscribe">
            <button className="subscribe-btn" onClick={() => handleLike(blogId)} disabled={dis}><img src={isLiked ? loveFill : love} alt="like" />({likes?.length})</button>
            <button className="comment-btn" onClick={() => navigate(`/blog/${blogId}`)} disabled={dis}><img src={comment} alt="comment" />({comments?.length})</button>
            <button className="subscribe-btn" onClick={() => {
                navigator.clipboard.writeText(`${process.env.REACT_APP_HOSTING}/blog/${blogId}`);
                toast.success("Link copied");
            }}><img src={share} alt="share" /></button>
        </div>
    );
}