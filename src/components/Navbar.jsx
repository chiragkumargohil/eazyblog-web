import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { pen } from "../assets";
import { useAuth } from '../context/AuthProvider';

export default function Navbar() {
    const { isAuth, logOutUser } = useAuth();
    const [menu, setMenu] = useState(false);
    const navigate = useNavigate();

    const handleMenu = () => {
        setMenu(!menu);
    };

    return (
        <nav id="navbar" className="flexed">
            <div className="logo">
                <Link to="/"><h1>EAZYBLOG</h1></Link>
            </div>
            <div className="profile">
                <div className="flexed avatar" onClick={handleMenu}><img src={pen} alt="pen" width="50%" /></div>
                {menu &&
                    <ul className='nav-labels'>
                        <li className="nav-links" onClick={() => setMenu(false)}><Link to="/">Home</Link></li>
                        <li className="nav-links" onClick={() => setMenu(false)}><Link to="/create-blog">Create Post</Link></li>
                        <li className="nav-links" onClick={() => setMenu(false)}><Link to="/profile">Profile</Link></li>
                        {isAuth ?
                        <>
                            <li className="nav-links" onClick={() => { setMenu(false); logOutUser(); navigate("/login") }}><p>Log Out</p></li>
                            <li className="nav-links"><span>{isAuth?.email}</span></li>
                        </>
                            :
                            <li className="nav-links" onClick={() => setMenu(false)}><Link to="/login">Log In to Create Post</Link></li>
                        }
                    </ul>
                }
            </div>
        </nav>
    );
}