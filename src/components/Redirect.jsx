import React from 'react'
import { Link } from 'react-router-dom';
import { done } from '../assets';

export default function Redirect({ message }) {
    return (
        <div id='redirect-page'>
            <p><img src={done} alt="done" color='green' /> Done!</p>
            <Link to="/">Go to Home Page</Link>
        </div>
    );
}