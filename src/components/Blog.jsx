import React from "react";
import { Link } from "react-router-dom";

import { code, read } from "../assets";
import LikeCommentBTN from "./LikeCommentBTN";
import {subStringBlog} from "../utils";

export default function Blog({ blogId, title, createdBy, createdOn, likes, comments }) {

    return (
        <div className="blog-card">
            <div className="flexed blog-category">
                <img src={code} alt="programming" />
                <p>EazyBlog</p>
            </div>

            <div className="flexed blog-title">
                <h3>{subStringBlog(title, 24)}</h3>
                <Link to={`/blog/${blogId}`}>
                    <img src={read} alt="read" title="Read Blog" />
                </Link>
            </div>

            <div className="blog-meta">
                <span>~ {createdBy}</span>
                <span>{createdOn}</span>
            </div>

            <LikeCommentBTN
                blogId={blogId}
                likes={likes}
                comments={comments}
            />
        </div>
    );
}