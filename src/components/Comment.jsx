import React from "react";

export default function Comment({ com }) {
    return (
        <div className="comment">
            <p>{com?.comment}</p>
            <div className="blog-meta">
                <span>~ {com?.displayName}</span>
                <span>{com?.dated}</span>
            </div>
        </div>
    );
}