import React from "react";

export default function SignupDialog({ value, handleChange, handleSubmit }) {
  return (
    <form onSubmit={handleSubmit}>
      <div className="inp-box">
        <label htmlFor="username">Name <span className="signup-note">(This name will be displayed to every post)</span></label>
        <input type="text" id="username" name="username" value={value.username} onChange={handleChange} placeholder="Name *" required />
      </div>

      <div className="inp-box">
        <label htmlFor="email">Email</label>
        <input type="text" id="email" name="email" value={value.email} onChange={handleChange} placeholder="Email *" required />
      </div>

      <div className="inp-box">
        <label htmlFor="password">Password</label>
        <input type="password" id="password" name="password" value={value.password} onChange={handleChange} placeholder="Password *" required />
      </div>

      <div className="inp-box">
        <label htmlFor="confirmPassword">Confirm Password</label>
        <input type="password" id="confirmPassword" name="confirmPassword" value={value.confirmPassword} onChange={handleChange} placeholder="Confirm Password *" required />
      </div>
      
      <div className="inp-box">
        <input type="submit" name="signup" value="Sign Up" />
      </div>
    </form>
  );
}