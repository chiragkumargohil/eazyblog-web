import React from "react";

export default function LoginDialog({ value, handleChange, handleSubmit }) {
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="inp-box">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" name="email" value={value.email} onChange={handleChange} placeholder="Email *" required />
        </div>

        <div className="inp-box">
          <label htmlFor="password">Password</label>
          <input type="password" id="password" name="password" value={value.password} onChange={handleChange} placeholder="Password *" required />
        </div>

        <div className="inp-box">
          <input type="submit" name="login" value="Log In" />
        </div>

      </form>
    </>
  );
}