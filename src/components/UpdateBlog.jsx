import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

import { updateBlog } from "../redux/actions/blogsAction";
import { modules } from "../contants";
import { currentDate } from "../utils";
import { useAuth } from "../context/AuthProvider";

export default function UpdateBlog({ blog, setDoRead }) {
    const { isAuth } = useAuth();
    const dated = currentDate();
    const dispatch = useDispatch();
    const [blogTitle, setBlogTitle] = useState(blog?.title);
    const [blogContent, setBlogContent] = useState(blog?.content);

    useEffect(() => {
        if (isAuth?.uid !== blog?.createdBy?.uid) {
            setDoRead(true)
        }
    }, [blog?.createdBy?.uid, isAuth?.uid]);

    const modifyBlog = async (e) => {
        e.preventDefault();
        const userBlog = {
            title: blogTitle,
            content: blogContent,
            createdOn: currentDate(),
            createdBy: { ...blog?.createdBy },
            likes: [...blog?.likes],
            comments: [...blog?.comments]
        };
        dispatch(updateBlog(blog?.id, userBlog));
        setDoRead(true);
    };

    return (
        <div className="create-blog">
            <div className="blog-header">
                <span className="blog-head">Update Blog</span>
                <span className="dated">{dated}</span>
            </div>
            <form onSubmit={modifyBlog}>
                <label className="blog-label">
                    <input type="text" name="blogTitle" id="blogTitle" value={blogTitle} onChange={(e) => setBlogTitle(e.target.value)} placeholder="Title" required />
                </label>
                <ReactQuill modules={modules} theme="snow" name="blogContent" id="blogContent" value={blogContent} onChange={(content, delta, source, editor) => setBlogContent(editor.getHTML())} placeholder="Content here..." required />
                <p className="created-by">~ {blog?.createdBy.displayName}</p>
                <label className="blog-label">
                    <input type="submit" name="update" value="Update" />
                </label>
                <label className="blog-label">
                    <input type="button" name="cancel" value="Cancel" onClick={() => setDoRead(true)} />
                </label>
            </form>
        </div>
    );
}