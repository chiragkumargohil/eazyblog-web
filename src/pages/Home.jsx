import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { Blog } from "../components";
import { useAuth } from "../context/AuthProvider";
import { getBlogs } from "../redux/actions/blogsAction";

export default function Home() {
	const { isAuth } = useAuth();
	const { blogs, liked } = useSelector(state => state.handleState);
	const dispatch = useDispatch();
	const [searching, setSearching] = useState("");

	useEffect(() => {
		dispatch(getBlogs());
	}, [dispatch, liked]);

	const filteredItems = blogs?.filter((blog) => blog.title.toLowerCase().includes(searching.toLowerCase()) || blog.createdBy.displayName.toLowerCase().includes(searching.toLowerCase()));

	return (
		<div id="all-blogs">

			<div className="hero">
				<div className="search-box">
					<input type="search" name="searching" value={searching} onChange={(e) => setSearching(e.target.value)} placeholder="Search blog..." />
					{searching &&
						<div className="searched">
							<p>{filteredItems.length} blog(s)</p>
							{filteredItems.map(filtered => (
								<Link key={filtered.id} to={`/blog/${filtered.id}`}>
									{filtered.title} ~ {filtered.createdBy.displayName}
								</Link>
							))}
						</div>
					}
				</div>

				<div className="btns">
					<a href="#reading">Start Reading</a>
					{!isAuth && <Link to={"/login"}>Login</Link>}
				</div>
			</div>

			<div id="reading" className="flexed the-blogs">
				{blogs.map((blog) => (
					<Blog
						key={blog.id}
						blogId={blog.id}
						title={blog.title}
						createdBy={blog.createdBy?.displayName}
						createdOn={blog.createdOn}
						likes={blog.likes}
						comments={blog.comments}
					/>
				))}
			</div>

		</div>
	);
}