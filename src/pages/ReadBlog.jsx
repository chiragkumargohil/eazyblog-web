import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { getBlog, removeBlog } from "../redux/actions/blogsAction";
import { Loader, UpdateBlog } from "../components";
import { useAuth } from "../context/AuthProvider";
import { AddComment, Comment, LikeCommentBTN } from "../components";
import { modify, trash } from "../assets";

export default function ReadBlog() {
    const { isAuth } = useAuth();
    const [doRead, setDoRead] = useState(true);
    const { id } = useParams();
    const { blog, commented, liked, loading } = useSelector(state => state.handleState);

    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(getBlog(id));
    }, [dispatch, id, liked, commented]);

    const handleDelete = () => {
        const userRes = window.confirm("The blog will be deleted permanently. Confirm ?");
        if (userRes) {
            dispatch(removeBlog(id));
            navigate("/redirect");
        }
    };

    return (
        <> {loading ? <Loader />
            :
            doRead ?
                <div className="full-post">
                    <div className="flexed blog-title">
                        <h1>{blog?.title}</h1>
                        {isAuth?.uid === blog?.createdBy?.uid &&
                            <div className="for-authed">
                                <p onClick={() => setDoRead(false)}>
                                    {isAuth &&
                                        <img src={modify} alt="modify" title="Modify Your Blog" />
                                    }
                                </p>
                                <p onClick={handleDelete}>
                                    {isAuth &&
                                        <img src={trash} alt="trash" title="Delete Your Blog" />
                                    }
                                </p>
                            </div>
                        }
                    </div>
                    <div className="blog-meta">
                        <span>~ {blog?.createdBy?.displayName}</span>
                        <span>{blog?.createdOn}</span>
                    </div>
                    <div dangerouslySetInnerHTML={{ __html: blog?.content }} className="post-content" />

                    <LikeCommentBTN
                        blogId={blog?.id}
                        isAuth={isAuth}
                        likes={blog?.likes}
                        comments={blog?.comments}
                    />

                    <div className="comments">
                        <AddComment isAuth={isAuth} id={blog?.id} />
                        {blog?.comments &&
                            blog?.comments?.reverse().map((com, index) => (
                                <Comment key={index} com={com} />
                            ))
                        }
                    </div>
                </div>
                :
                <UpdateBlog blog={blog} setDoRead={setDoRead} />
        }
        </>
    );
}