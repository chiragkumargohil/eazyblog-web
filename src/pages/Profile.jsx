import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { useAuth } from "../context/AuthProvider";
import { pen } from "../assets";
import { Blog, Loader } from "../components";
import { getUserBlogs } from "../redux/actions/blogsAction";

export default function Profile() {
    const { isAuth } = useAuth();
    const dispatch = useDispatch();
    const { userBlogs, liked, loading } = useSelector(state => state.handleState);
    const userId = isAuth.uid;

    useEffect(() => {
        dispatch(getUserBlogs(userId));
    }, [dispatch, userId, liked]);

    return (<>
        {loading ? <Loader /> :
            <div id="profile-page">
                <div className="flexed user-img">
                    <img src={pen} alt="user" />
                </div>
                <div className="user-detail">
                    <h4>{isAuth.displayName}</h4>
                </div>
                <hr />
                <div className="flexed the-blogs">
                    {userBlogs?.map((blog) => (
                        <Blog
                            key={blog.id}
                            blogId={blog.id}
                            title={blog.title}
                            createdBy={blog.createdBy.displayName}
                            createdOn={blog.createdOn}
                            likes={blog.likes}
                            comments={blog.comments}
                        />
                    ))}
                </div>
            </div>
        }
    </>
    );
}