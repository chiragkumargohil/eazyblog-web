import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
// import { toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";

import { LoginDialog, SignupDialog } from "../components/Dialogs";
// import { toastDefault } from "../contants";
import { useAuth } from "../context/AuthProvider";

// toast.configure();
const logInInitial = {
    email: "",
    password: ""
};

const sigUpInitial = {
    username: "",
    email: "",
    password: "",
    confirmPassword: ""
};

export default function Login() {
    const [isLogin, setIsLogin] = useState(true);
    const [logInValue, setLogInValue] = useState(logInInitial);
    const [signUpValue, setSignUpValue] = useState(sigUpInitial);
    const [forgotEmail, setForgotEmail] = useState("");
    const [open, setOpen] = useState(false)
    const { logInUser, signUpUser, resetPassword } = useAuth();
    const navigate = useNavigate();

    const logInChange = (e) => {
        setLogInValue({ ...logInValue, [e.target.name]: e.target.value });
    }

    const logIn = async (e) => {
        e.preventDefault();
        try {
            await logInUser(logInValue.email, logInValue.password);
            navigate("/");
        } catch (error) {
            toast.error("Wrong Email / Password");
        }
        setLogInValue(logInInitial);
    }

    const signUpChange = (e) => {
        setSignUpValue({ ...signUpValue, [e.target.name]: e.target.value });
    }

    const signUp = async (e) => {
        e.preventDefault();
        if (signUpValue.password !== signUpValue.confirmPassword) {
            setSignUpValue({ ...signUpValue, password: "", confirmPassword: "" });
            toast.error("Password doesn't match.");
        }

        try {
            await signUpUser(signUpValue.username, signUpValue.email, signUpValue.password);
            navigate("/");
        } catch (error) {
            toast.error("Something went wrong. Please try after some time.");
        }
        setSignUpValue(sigUpInitial);
    }

    const forgotPassword = async (e) => {
        e.preventDefault();
        try {
            await resetPassword(forgotEmail);
            toast.success("Email sent to your email.");
            setOpen(false)
        } catch (error) {
            toast.error("Something went wrong. Please try after some time.");
        }
    }

    const cleanUp = () => {
        setLogInValue(logInInitial);
        setSignUpValue(sigUpInitial);
    }

    return (
        <div id='login'>
            <div className='login-header'>
                <button type='button' onClick={() => { setIsLogin(true); cleanUp() }} style={{ borderBottom: isLogin && "4px solid brown" }}>Log In</button>
                <button type='button' onClick={() => { setIsLogin(false); cleanUp() }} style={{ borderBottom: !isLogin && "4px solid brown" }}>Sign Up</button>
            </div>
            <div className="login-container">
                {isLogin ?
                    <>
                        {open ?
                            <form onSubmit={forgotPassword}>
                                <div className="inp-box">
                                    <label htmlFor="forgotmail">Email</label>
                                    <input type="email" id="forgotmail" name="forgotmail" value={forgotEmail} onChange={(e) => setForgotEmail(e.target.value)} placeholder="Email *" required />
                                </div>
                                <div className="inp-box">
                                    <input type="submit" name="forgotpass" value="Send a Mail" />
                                </div>
                            </form>
                            :
                            <LoginDialog value={logInValue} handleChange={logInChange} handleSubmit={logIn} open={open} setOpen={setOpen} />
                        }
                        <div className="forgot-btn">
                            <button type="button" className="forgot-pass" onClick={() => setOpen(!open)}>{!open ? "Forgot Password?" : "Back to Login"}</button>
                        </div>
                    </>
                    :
                    <SignupDialog value={signUpValue} handleChange={signUpChange} handleSubmit={signUp} />
                }
            </div>
        </div>
    );
}