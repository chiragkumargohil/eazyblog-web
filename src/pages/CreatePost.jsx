import React, { useState } from "react";
import ReactQuill from "react-quill";
import 'react-quill/dist/quill.snow.css';
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { createDoc } from "../redux/actions/blogsAction";
import { currentDate } from "../utils";
import { modules } from "../contants";
import { useAuth } from "../context/AuthProvider";

export default function CreatePost() {
    const { isAuth } = useAuth();
    const dated = currentDate();
    const [blogTitle, setBlogTitle] = useState("");
    const [blogContent, setBlogContent] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const createBlog = async (e) => {
        e.preventDefault();
        const userBlog = {
            title: blogTitle,
            content: blogContent,
            createdOn: currentDate(),
            createdBy: {
                displayName: isAuth.displayName,
                uid: isAuth.uid
            },
            likes: [],
            comments: []
        };
        dispatch(createDoc(userBlog));
        navigate("/redirect");
    };

    return (
        <div className="create-blog">
            <div className="blog-header">
                <span className="blog-head">Create Blog</span>
                <span className="dated">{dated}</span>
            </div>
            <form onSubmit={createBlog}>
                <label className="blog-label">
                    <input type="text" name="blogTitle" id="blogTitle" value={blogTitle} onChange={(e) => setBlogTitle(e.target.value)} placeholder="Title" required />
                </label>
                <ReactQuill modules={modules} theme="snow" name="blogContent" id="blogContent" value={blogContent} onChange={(content, delta, source, editor) => setBlogContent(editor.getHTML())} placeholder="Content here..." required />
                <p className="created-by">~ {isAuth.displayName}</p>
                <label className="blog-label">
                    <input type="submit" name="publish" value="Publish" />
                </label>
            </form>
        </div>
    );
}