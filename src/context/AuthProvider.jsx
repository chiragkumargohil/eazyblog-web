import React, { useContext, createContext, useState, useEffect } from "react";
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, updateProfile, onAuthStateChanged, sendPasswordResetEmail } from "firebase/auth";

import { auth } from "../database";

const AuthContext = createContext();

export default function AuthProvider({ children }) {
    const [isAuth, setIsAuth] = useState();

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setIsAuth(user);
            } else {
                setIsAuth();
            }
        });
    }, []);

    const signUpUser = async (name, email, password) => {
        const createdUser = await createUserWithEmailAndPassword(auth, email, password);
        await updateProfile(auth.currentUser, { displayName: name });
        return createdUser;
    };

    const logInUser = async (email, password) => {
        const loggedUser = await signInWithEmailAndPassword(auth, email, password);
        return loggedUser;
    };

    const logOutUser = () => {
        return signOut(auth);
    };

    const resetPassword = async (email) => {
        await sendPasswordResetEmail(auth, email);
    }

    const values = { isAuth, signUpUser, logInUser, logOutUser, resetPassword };
    return (
        <AuthContext.Provider value={values}>
            {children}
        </AuthContext.Provider>
    );
}

export const useAuth = () => useContext(AuthContext);