import React from "react";
import { Navigate } from "react-router-dom";

import { useAuth } from "./AuthProvider";

export default function ({children}) {
    const {isAuth} = useAuth();
    if (!isAuth) {
        return <Navigate to="/login" />
    }
    return children;
}