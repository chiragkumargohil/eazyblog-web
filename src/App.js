import React from 'react';
import { Route, Routes } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';

import './App.css';
import { CreatePost, Home, Login, ReadBlog, Profile } from './pages';
import { Navbar, Redirect } from './components';
import ProtectedRoute from './context/ProtectedRoute';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/create-blog'
          element={
            <ProtectedRoute>
              <CreatePost />
            </ProtectedRoute>
          } />
        <Route path='/profile'
          element={
            <ProtectedRoute>
              <Profile />
            </ProtectedRoute>
          } />

        <Route path='/blog/:id' element={<ReadBlog />} />
        <Route path='/redirect' element={<Redirect />} />
      </Routes>
      <ToastContainer />
    </>
  );
}

export default App;