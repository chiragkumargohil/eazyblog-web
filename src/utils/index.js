export const currentDate = () => {
    const createdOn = new Date();
    const yyyy = createdOn.getFullYear();
    let mm = createdOn.getMonth() + 1;
    let dd = createdOn.getDate();
    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;

    return dd + "/" + mm + "/" + yyyy;
};

export const subStringBlog = (content, upto) => {
    if (content.length < upto) return content;
    return content.substring(0, upto) + "...";
};