// LOADING
export const LOADING = "loading";

// ERROR
export const ERROR = "error";

// FOR ALL BLOGS
export const GET_ALL_BLOGS = "getAllBlogs";

// FOR PARTICULAR BLOG
export const GET_BLOG = "getBlog";

// CREATE BLOG
export const CREATE_BLOG = "createBlog";

// UPDATE BLOG
export const UPDATE_BLOG = "updateBlog";

// DELETE BLOG
export const REMOVE_BLOG = "removeBlog";

// LIKE BLOG
export const LIKE_BLOG = "likeBlog";

// DISLIKE BLOG
export const DISLIKE_BLOG = "dislikeBlog";

// COMMENT ON BLOG
export const ADD_COMMENT = "addComment";
export const DELETE_COMMENT = "deleteComment";

// USER BLOG
export const GET_USER_BLOGS = "getUserBlogs";