import * as actionType from "../constants";

const initialState = {
    loading: true,
    blogs: [],
    deletedId: undefined,
    userBlogs: [],
    blog: {},
    error: "",
    liked: undefined,
    commented: undefined
}

export const blogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.LOADING:
            return { ...state, loading: true };

        case actionType.GET_ALL_BLOGS:
            return { ...state, blogs: action.payload, loading: false };

        case actionType.GET_BLOG:
            return { ...state, blog: action.payload, loading: false };

        case actionType.GET_USER_BLOGS:
            return { ...state, userBlogs: action.payload, loading: false };

        case actionType.CREATE_BLOG:
            return { ...state, loading: false };

        case actionType.UPDATE_BLOG:
            return { ...state, blog: action.payload, loading: false };

        case actionType.REMOVE_BLOG:
            return { ...state, deletedId: action.payload, loading: false };

        case actionType.LIKE_BLOG:
            return { ...state, liked: action.payload, loading: false };

        case actionType.DISLIKE_BLOG:
            return { ...state, liked: action.payload, loading: false };

        case actionType.ADD_COMMENT:
            return { ...state, blog: { ...state.blog, comments: [action.payload, ...state.blog.comments] }, loading: false };

        case actionType.ERROR:
            return { ...state, error: action.payload, loading: false };

        default:
            return state;
    }
};