import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

// import { createDocReducer, dislikeBlogReducer, getBlogReducer, getBlogsReducer, likeBlogReducer, removeBlogReducer, updateBlogReducer } from "./reducers/blogsReducer";
import { blogsReducer } from "./reducers/reducer";

const reducers = combineReducers({
    handleState: blogsReducer
});

const middleware = [thunk];

const store = createStore(reducers, composeWithDevTools(applyMiddleware(...middleware)));

export default store;