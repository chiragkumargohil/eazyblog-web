import { doc, getDoc, getDocs, addDoc, setDoc, deleteDoc, updateDoc, arrayUnion, arrayRemove, query, where, collection } from "firebase/firestore";

import * as actionType from "../constants";
import { blogsCollection, db } from "../../database";

export const getBlogs = () => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const querySnapshot = await getDocs(blogsCollection);
        dispatch({ type: actionType.GET_ALL_BLOGS, payload: querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })) });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const getBlog = (id) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });

        const docRef = doc(db, "blogs", id);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
            dispatch({ type: actionType.GET_BLOG, payload: { id, ...docSnap.data() } });
        } else {
            dispatch({ type: actionType.ERROR, payload: "ITEM_NOT_FOUND" });
        }
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const createDoc = (data) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const doc = await addDoc(blogsCollection, data);
        dispatch({ type: actionType.CREATE_BLOG, payload: { id: doc.id, ...data } });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const updateBlog = (id, data) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const docRef = doc(db, "blogs", id);
        await setDoc(docRef, data);
        dispatch({ type: actionType.UPDATE_BLOG, payload: { id, ...data } });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const removeBlog = (id) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const docRef = doc(db, "blogs", id);
        await deleteDoc(docRef);
        dispatch({ type: actionType.REMOVE_BLOG, payload: id });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const likeBlog = (id, userId) => async (dispatch) => {
    try {
        const likesRef = doc(db, "blogs", id);
        await updateDoc(likesRef, {
            likes: arrayUnion(userId)
        });
        dispatch({ type: actionType.LIKE_BLOG, payload: { id, liked: true } });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const dislikeBlog = (id, userId) => async (dispatch) => {
    try {
        const likesRef = doc(db, "blogs", id);
        await updateDoc(likesRef, {
            likes: arrayRemove(userId)
        });
        dispatch({ type: actionType.DISLIKE_BLOG, payload: { id, liked: false } });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const addComment = (id, data) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const commentsRef = doc(db, "blogs", id);
        await updateDoc(commentsRef, {
            comments: arrayUnion(data)
        });
        dispatch({ type: actionType.ADD_COMMENT, payload: data });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};

export const getUserBlogs = (userId) => async (dispatch) => {
    try {
        dispatch({ type: actionType.LOADING });
        const q = query(collection(db, "blogs"), where("createdBy.uid", "==", userId));
        const querySnapshot = await getDocs(q);
        dispatch({ type: actionType.GET_USER_BLOGS, payload: querySnapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })) });
    } catch (error) {
        dispatch({ type: actionType.ERROR, payload: error.response });
    }
};