import pen from "./svg/pen.svg";
import love from "./svg/love.svg";
import modify from "./svg/modify.svg";
import read from "./svg/read.svg";
import comment from "./svg/comment.svg";
import bookmark from "./svg/bookmark.svg";
import share from "./svg/share.svg";
import trash from "./svg/trash.svg";
import loveFill from "./svg/loveFill.svg";
import search from "./svg/search.svg";
import code from "./svg/categories/code.svg";
import done from "./svg/done.svg";

export { pen, love, modify, read, bookmark, share, comment, trash, loveFill, search, code, done };